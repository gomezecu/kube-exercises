# Ejercicio 2

## Punto 1

1. Declaramos un servicio para los pods de mongodb que crearemos más adelante:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: mongodb-service
  labels:
    name: mongo
spec:
  ports:
  - port: 27017
    targetPort: 27017
  clusterIP: None
  selector:
    role: mongo
```

2. Creamos el servicio:

```shell
kubectl apply -f mongo-service.yml
```

3. Declaramos un StatefulSet con tres instancias de MongoDB:

```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: mongod
spec:
  serviceName: mongodb-service
  replicas: 3
  selector:
    matchLabels:
      role: mongo
  template:
    metadata:
      labels:
        role: mongo
        replicaset: myReplSet
    spec:
      terminationGracePeriodSeconds: 10
      containers:
        - name: mongod-container
          image: mongo
          command:
            - "mongod"
            - "--bind_ip"
            - "0.0.0.0"
            - "--replSet"
            - "myReplSet"
          resources:
            requests:
              cpu: 0.2
              memory: 200Mi
          ports:
            - containerPort: 27017
          volumeMounts:
            - name: mongodb-persistent-storage-claim
              mountPath: /data/db
  volumeClaimTemplates:
    - metadata:
        name: mongodb-persistent-storage-claim
      spec:
        accessModes: [ "ReadWriteOnce" ]
        resources:
          requests:
            storage: 1Gi
```

4. Creamos el StatefulSet:

```shell
kubectl apply -f mongo-statefulset.yml
```

5. Confirmamos que tenemos nuestro cluster con lo que necesitamos:

![](img/21.png)

6. Ahora que tenemos los 3 pods de MongoDB, tenemos que entrar al primero de ellos (a través del nombre que vemos arriba):

```shell
k exec -it mongod-0 -- bash
```

7. Miramos el hostname para utilizarlo más adelante:

```shell
root@mongod-0:/# hostname -f
```

![](img/22.png)

8. Una vez que tenemos el hostname, lanzamos el shell con el comando `mongosh`; hacemos esto para poder lanzar un comando de mongodb que 
   nos cree un replica set (de mongo) de tres instancias de `mongod` (básicamente, con esto MongoDB nos ayudará a trabajar con esta base de 
   datos distribuida que hemos creado, temas de consistencia de datos, acceso a estos, elecciones de los nodos, etc.):

```shell
# Entramos al shell de mongodb
mongosh

# Creamos el replica set indicando los nombres correctos de los host y el puerto
rs.initiate(
   {
      _id: "myReplSet",
      version: 1,
      members: [
         { _id: 0, host : "mongod-0.mongodb-service.default.svc.cluster.local:27017" },
         { _id: 1, host : "mongod-1.mongodb-service.default.svc.cluster.local:27017" },
         { _id: 2, host : "mongod-2.mongodb-service.default.svc.cluster.local:27017" }
      ]
   }
);
```

9. Podemos lanzar el comando `rs.status()` para ver el RS creado:

![](img/23.png)

10. Podemos incluso crear un usuario admin en la database correspondiente:

```shell
db.getSiblingDB("admin").createUser({ user : "eze", pwd  : "abc123", roles: [ { role: "root", db: "admin" } ]});
```

11. Aquí adjuntamos la serie de procesos para comprobar que el usuario fue añadido en todas las instancias:

NOTA: Al usar un replicaSet de mongo, todas las réplicas secundarias tienen los mismos datos que la primera, pero para poder leer datos 
de las secundarias tenemos que indicárselo explícitamente a mongo lanzando el comando `rs.secondaryOk()` desde cualquier réplica 
secundaria (esto es para garantizar una mejor consistencia de lectura de datos, pues los secundarios tardan un poco más en tener datos 
nuevos que con respecto al primario). En mi caso, usé otro comando parecido: `db.getMongo().setReadPref("primaryPreferred")`.

#### Instancia 0
![](img/24.png)

#### Instancia 1
![](img/25.png)

## StatefulSet vs ReplicaSet

El uso del StatefulSet frente al de ReplicaSet es para que nuestro servicio de MongoDb tenga un identificador específico y fácilmente 
identificable, pues el StatefulSet asignará, en vez de un hash aleatorio, un número secuencial a medida que los pods del servicio se van 
replicando (escalando). Por otro lado, RS y Deployments se usan para aplicaciones stateless, mientras que StatefulSet, como su nombre 
indica, es para aplicaciones que necesitan ciertos datos de manera persistente. En nuestro ejemplo, vemos que podemos crear un 
replicaSet de Mongo fácilmente porque sabemos los identificadores de todos los pods. Por otra parte, con el StatefulSet los nodos se 
crean siguiendo un orden, mientras que en el ReplicaSet es aleatorio. Finalmente, en caso de que se muera un pod, el nuevo que lo 
sustituye tendrá el mismo ID. Esto otorga consistencia de datos y la posibilidad de tener una estructura de master/slave.
