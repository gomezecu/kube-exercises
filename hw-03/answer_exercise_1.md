# Ejercicio 1

## Parte A

1. Creamos un ReplicaSet object con las especificaciones requeridas:

```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: rs1
  labels:
    app: nginx-server
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
        - name: nginx
          image: nginx:1.19.4-alpine
          imagePullPolicy: Always
          ports:
            - containerPort: 80
          resources:
            requests:
              cpu: "20m"
              memory: "128Mi"
            limits:
              cpu: "20m"
              memory: "128Mi"
```

2. Creamos un servicio:

````yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx
spec:
  type: ClusterIP
  selector:
    app: nginx-server
  ports:
    - name: http
      port: 80
      targetPort: 80
      protocol: TCP
````

3. Creamos un Ingress:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: ingress1
spec:
  rules:
    - host: egomez.student.lasalle.com
      http:
        paths:
          - pathType: Prefix
            path: /
            backend:
              service:
                name: nginx
                port:
                  number: 80
```

4. Lanzamos los siguientes comandos para crear los objetos:

```shell
kubectl apply -f rs1.yml
kubectl apply -f service1.yml
kubectl apply -f ingress1.yml
```

![](img/1.png)

![](img/2.png)

5. Obtenemos la IP de nuestro cluster de Minikube

![](img/3.png)

6. Utilizamos "Gas Mask" para editar la info de nuestro host y vincular la ip del cluster con nuestro "fake" domain

![](img/4.png)

7. Confirmamos que tenemos instalado el addons de ingress en Minikube (lo podemos añadir con el comando `minikube addons enable ingress`)

![](img/6.png)

9. Accedemos al dominio desde nuestro browser y vemos nuestro nginx corriendo:

![](img/5.png)

## Parte B

1. Declaramos un Secret:

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: nginx-secret
  namespace: default
type: Opaque
stringData:
  config.yaml: |-
  username: ezequiel
  password: abcd1234
```

2. Lo lanzamos al Cluster:

```shell
kubectl apply -f secret.yml
```

3. Modificamos nuestro objeto Ingress, donde le indicamos tanto nuestro host url y el secret previamente creado:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: ingress1
spec:
  tls:
  - hosts:
    - egomez.student.lasalle.com
    secretName: nginx-secret
  rules:
    - host: egomez.student.lasalle.com
      http:
        paths:
          - pathType: Prefix
            path: /
            backend:
              service:
                name: nginx
                port:
                  number: 80
```

4. Generamos una CA private key con OpenSSL:

```shell
OpenSSL genrsa -out ca.key 2048
```

5. Creamos un certificado auto firmado valido por 7 días indicando como subject nuestro domain:

```shell
openssl req -x509 \
  -new -nodes  \
  -days 7 \
  -key ca.key \
  -out ca.crt \
  -subj "/CN=egomez.student.lasalle.com"
```

6. Comprobamos que funciona:

![](img/11.png)

![](img/12.png)
