# Ejercicio 3

1. Partimos de un Deployment basado en ReplicaSet del primer ejercicio:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
   name: nginx-deploy
   labels:
      app: nginx-server
spec:
   replicas: 3
   selector:
      matchLabels:
         app: nginx-server
   template:
      metadata:
         labels:
            app: nginx-server
      spec:
         containers:
            - name: nginx
              image: nginx:1.19.4-alpine
              imagePullPolicy: Always
              ports:
                 - containerPort: 80
              resources:
                 limits:
                    cpu: 125m
                 requests:
                    cpu: 50m
```

2. Lo ejecutamos en nuestro Cluster:

```shell
kubectl apply -f rs1.yaml
```

![](img/31.png)

3. Creamos un NodePort para poder acceder desde el exterior:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx
spec:
  type: NodePort
  selector:
    app: nginx-server
  ports:
    - nodePort: 31111
      port: 80
      targetPort: 80
      protocol: TCP
```

4. Lo ejecutamos en nuestro Cluster:

```shell
kubectl apply -f nodeport.yaml
```

5. Un HPA funciona porque hay un endpoint (`/metrics`) que tiene el controlador y que da métricas de los mismos pods activos (uso de 
   recursos, etc.). En minikube podemos instalar este servicio habilitando el addon `metrics-server`:

![](img/34.png)

7. Para crear un HorizontalPodAutoscaler usaremos la manera imperativa haciendo uso del siguiente comando:

```shell
kubectl autoscale rs rs1 --min=3 --max=6 --cpu-percent=50
```

O bien de manera declarativa:

```yaml
apiVersion: autoscaling/v2beta2
kind: HorizontalPodAutoscaler
metadata:
   name: nginx-hpa
   namespace: default
spec:
   scaleTargetRef:
     apiVersion: apps/v1
     kind: Deployment
     name: nginx-deploy
   minReplicas: 3
   maxReplicas: 6
   metrics:
   - type: Resource
     resource:
       name: cpu
       target:
        type: Utilization
        averageUtilization: 50
```

7. Lanzamos el siguiente comando para estresar el servicio (usar la IP correspondiente del cluster de minikube):

```shell
kubectl run -i --tty load-generator --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://192.168.64.4:31111/; done"
```

8. Podemos ver como el uso de CPU va aumentando:

![](img/32.png)

10. Cuando supere el 50% objetivo podremos comprobamos cómo nuestros pods comienzan a escalar poco a poco:

![](img/33.png)
