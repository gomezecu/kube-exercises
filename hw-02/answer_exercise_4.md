# Ejercicio 4

El archivo para el deployment sería así:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx-server
spec:
  replicas: 5
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
      - name: nginx
        image: nginx:1.19.4-alpine
        imagePullPolicy: Always
        ports:
        - containerPort: 80
```

Si lanzamos este Deployment con `kubectl apply -f deplyment.yml`, veremos cómo los 3 pods que estaban running son destruidos y 5 nuevos
creados desde cero (podemos confirmar que ninguna coincide con viejas IDs):

![](img/15.png)

Si quisiéramos utilizar una estrategia de `rollout` deployment que indique un máximo de 2 pods que puedan ser añadidos al mismo tiempo y
que 0 pods pueden estar no disponibles durante el rolling update, este sería el archivo necesario:

NOTA: cambiamos la versión de la imagen para que se detecte un cambio (nginx:1.19.3-alpine) y aplique el deployment nuevo mediante el
comando de siempre, a saber, `kubectl apply -f deployment-rollout.yml`. También podemos usar un comando tipo `kubectl set image
deployment/nginx-deployment nginx=nginx:1.19.3-alpine`. Podemos añadirle `--record` para ver registrado el comando lanzado en el
histórico de revisiones.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx-server
spec:
  replicas: 5
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 2
      maxUnavailable: 0
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
      - name: nginx
        image: nginx:1.19.3-alpine
        imagePullPolicy: Always
        ports:
        - containerPort: 80
```

Ahora bien, queremos hacer un Rollback a la versión anterior, es decir, a la 1.19.4. Para ello lo hacemos de la siguiente manera:

- Primero podemos lanzar un listado de los ReplicaSet que tenemos:

![](img/16.png)

- Vemos que aún tenemos un ID del anterior utilizado, es decir, el que tenía la versión a la que queremos hacer un rollback. Podemos verlo
  con un `describe`:

![](img/17.png)

- Podemos usar el comando `kubectl rollout history deploy nginx-deployment` pare tener un listado de las "revisions":

![](img/19.png)

- Vemos 2, por lo que para hacer un rollout a la primera usaremos el siguiente comando:

```shell
kubectl rollout undo deploy nginx-deployment --to-revision=1
```

![](img/20.png)

![](img/21.png)

- Si usamos el ID de alguno de los nuevos pods y lanzamos un describe, veremos que ahora están usando de nuevo la versión 1.19.4:

![](img/22.png)