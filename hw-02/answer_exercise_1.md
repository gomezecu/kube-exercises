# Exercise 1

Primero definimos el archivo declarativo para el pod:

```yaml
apiVersion: v1
kind: Pod
metadata:
  app: nginx-server
spec:
  containers:
  - name: nginx
    image: nginx:1.19.4-alpine
    ports:
    - containerPort: 80
```

A continuación, desplegamos en el cluster el pod mediante el siguiente comando:

```shell
kubectl apply -f pod.yml
```

Vemos como el pod ha sido correctamente creado y deployado en nuestro cluster:

![](img/01.png)

Podemos obtener información del pod usando:

```shell
kubectl get pods -o wide
```

![](img/02.png)

Esto nos sirve para obtener el nombre del pod, el número IP generado, etc.

NOTA: También podríamos usar el comando `kubectl describe pod nginx`, donde veríamos mucha más información sobre el pod en caso de
necesitarla (como anotaciones, información de los contenedores, etc.)

Para ver las diez últimas entradas de logs de nuestro pod, podemos usar el siguiente comando:

```shell
kubectl logs --tail=10 nginx
```

![](img/03.png)

Si queremos entrar dentro del pod:

```shell
kubectl exec -it nginx -- sh
```

![](img/04.png)

Una vez dentro del container podemos visualizar el contenido que expone nginx mediante una llamada via `curl`:

![](img/05.png)

Si quisiéramos acceder desde nuestro host, podemos usar un port forward:

```shell
kubectl port-forward pods/nginx 8080:80
```

![](img/06.png)

![](img/07.png)

Finalmente, podemos mirar la calidad del servicio establecida en el pod mirando la info del pod en el campo status

```shell
kubectl get pod nginx -o=json | jq -r '.status.qosClass'
```

En nuestro caso, como el pod cumple con ciertos criterios establecidos en el `yaml`, vemos un `QoS` de `Guaranteed`:

![](img/08.png)
