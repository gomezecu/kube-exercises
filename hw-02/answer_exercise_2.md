# Exercise 2

Primero definimos el archivo declarativo para el ReplicaSet:

````yaml
apiVersion: v1
kind: ReplicaSet
metadata:
  name: nginx
  labels:
    app: nginx-server
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
        - name: nginx
          image: nginx:1.19.4-alpine
          ports:
            - containerPort: 80
          resources:
            requests:
              memory: "256Mi"
              cpu: "100m"
            limits:
              memory: "256Mi"
              cpu: "100m"
````

Hacemos deploy del ReplicaSet:

```shell
kubectl apply -f replicaSet.yml
```

Para escalar el número de réplicas usaría el comando `kubectl autoscale`:

````shell
kubectl autoscale rs nginx --min=10 --max=10
````

![](img/09.png)

![](img/10.png)

Si necesito tener una réplica en cada uno de los nodos de Kubernetes, el objeto que se adaptaría mejor para esto sería creo que sería el
propio `Pod`.