# Ejercicio 3

## Exponiendo el servicio hacia el exterior

Para este caso usaríamos un `LoadBalancer`;

- Primero crearíamos un cluster en algún cloud provider tipo Google Cloud

Una vez dentro del cluster, podemos lanzar el siguiente comando para que nos genere un servicio del tipo LoadBalancer:

```shell
kubectl expose pod nginx --name service-lb --type=LoadBalancer --port=80
```

![](img/23.png)

Si tuviéramos un cluster en la nube, entonces veríamos una IP pública asignada a nuestro servicio, no localhost.

Como hemos creado el servicio de manera imperativa, podemos lanzar `kubectl get svc service-lb -o yaml` para obtener el servicio en
formato `yaml`. Sería el siguiente "resumido":

```yaml
apiVersion: v1
kind: Service
metadata:
  labels:
    app: nginx-server
  name: service-lb
spec:
  ports:
  - nodePort: 31360
    port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: nginx-server
  type: LoadBalancer
```

## De forma interna, sin acceso desde el exterior

Usamos `ClusterIP` (que es el tipo por defecto si no lo especificamos):

```yaml
# service2.yml
apiVersion: v1
kind: Service
metadata:
  name: service-clusterip
spec:
  type: ClusterIP
  selector:
    app: nginx-server
  ports:
    - name: http
      port: 80
      targetPort: 80
      protocol: TCP
```

## Abriendo un puerto específico de la VM

Usamos `NodePort`:

```yaml
# service3.yml
apiVersion: v1
kind: Service
metadata:
  name: service-nodeport
spec:
  type: NodePort
  selector:
    app: nginx-server
  ports:
    - nodePort: 31111
      port: 80
      targetPort: 80
      protocol: TCP
```

Creamos el servicio con el comando:

```shell
kubectl apply -f service1.yml
```

![](img/11.png)

![](img/12.png)

Podemos comprobar que tenemos el servicio en nuestro cluster via `kubectl get svc`:

![](img/14.png)

Ahora podemos acceder desde nuestro localhost indicando el puerto `31111`:

![](img/13.png)

NOTA: En este punto tenía problemas con Minikube, por lo que use el cluster de docker que me permite acceder desde el localhost. Si
quisiera acceder teniendo el cluster con Minikube, haría un `minikube ip` para obtener la IP del cluster y acceder por ella en vez de
localhost.

NOTA: Podemos usar la forma imperativa para crear el servicio, usando el comando `kubectl expose pod` para que la IP expuesta externa
esté controlada por Kubernetes (y no tengamos que tener en cuenta nosotros si determinado puerto podemos usar o no).