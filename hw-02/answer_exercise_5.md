# Ejercicio 5

Para establecer una estrategia de deployment Blue-Green, lo haría de la siquiente manera:

1. Añadiría al deployment anterior una nueva label "color=blue":
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: deployment-bg
  labels:
    app: nginx-server
    color: blue
spec:
  replicas: 1
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
      - name: nginx
        image: nginx:1.19.4-alpine
        imagePullPolicy: Always
        ports:
        - containerPort: 80
```

2. Añadiría al servicio de ClusterIP un nuevo selector "color=blue":

```yaml
apiVersion: v1
kind: Service
metadata:
  name: service-clusterip
spec:
  type: ClusterIP
  selector:
    app: nginx-server
    color: blue
  ports:
    - name: http
      port: 80
      targetPort: 80
      protocol: TCP
```

3. Haría el deployment con `kubectl apply -f deployment-bg.yaml`

4. Ahora vendría la parte emocionante, le modificaría al deployment la version de nuestra imagen y le cambiaríamos el color a "green":

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: deployment-bg
  labels:
    app: nginx-server
    color: green
spec:
  replicas: 1
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
      - name: nginx
        image: nginx:1.20.1-alpine
        imagePullPolicy: Always
        ports:
        - containerPort: 80
```

5. Haría un nuevo deployment con `kubectl apply -f deployment-bg.yaml`
6. En este momento no pasaría nada porque los pods no matchean con las labels del servicio load balancer creado
7. Por lo que para pasar de blue a green simplemente cambiaríamos el selector del servicio, en este caso, para variar un poco, podemos
   hacerlo de manera imperativa con el siguiente comando: `kubectl patch svc service-clusterip -p '{"spec":{"selector":{"color": "green"}}}'`

![](img/30.png)

Y así podemos pasar de una versión a otra todos los pods con un simple comando.


